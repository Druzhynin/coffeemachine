//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Kirill Druzhynin on 16.06.2018.
//  Copyright © 2018 Kirill Druzhynin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() { 
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        addAllIngridients()
        makeCoffee(coffeeType: americanoWithMilk)
        showLevels()
        makeCoffee(coffeeType: americanoWithMilk)
        showLevels()
        makeCoffee(coffeeType: americanoWithoutMilk)
        showLevels()
        makeCoffee(coffeeType: cappucino)
        showLevels()
        makeCoffee(coffeeType: cappucino)
        showLevels()
        makeCoffee(coffeeType: cappucino)
        showLevels()
        addMilk()
        makeCoffee(coffeeType: cappucino)
        showLevels()
    }
    
    var waterLevel: Int = 0
    var milkLevel: Int = 0
    var beansLevel: Int = 0
    var dregdrawerLevel = 0
    let maxDregdrawerLevel = 10
    let americanoWithoutMilk = CoffeeTypes(coffeeName: "Americano without milk", waterNeeded: 200, milkNeeded: 0, beansNeeded: 8, trashProduced: 1)
    let americanoWithMilk = CoffeeTypes(coffeeName: "Americano with milk", waterNeeded: 150, milkNeeded: 50, beansNeeded: 8, trashProduced: 1)
    let cappucino = CoffeeTypes(coffeeName: "Cappucino", waterNeeded: 100, milkNeeded: 200, beansNeeded: 8, trashProduced: 1)
    
    func makeCoffee(coffeeType: CoffeeTypes) {
        if waterLevel >= coffeeType.waterNeeded && milkLevel >= coffeeType.milkNeeded && beansLevel >= coffeeType.beansNeeded && dregdrawerLevel < maxDregdrawerLevel {
            print("""
                ================================================
                Here is your \(coffeeType.coffeeName)! Enjoy it!
                ================================================
                """)
            waterLevel -= coffeeType.waterNeeded
            milkLevel -= coffeeType.milkNeeded
            beansLevel -= coffeeType.beansNeeded
            dregdrawerLevel += coffeeType.trashProduced
        } else {
            if waterLevel < coffeeType.waterNeeded {
                print("Water level is too low! To get your \(coffeeType.coffeeName) add some water!")
            } else if milkLevel < coffeeType.milkNeeded {
                print("Milk level is too low! To get your \(coffeeType.coffeeName) add some milk!")
            } else if beansLevel < coffeeType.beansNeeded {
                print("Out of beans! To get your \(coffeeType.coffeeName) add some beans!")
            } else if dregdrawerLevel < maxDregdrawerLevel {
                print("Please clean the dregdrower to get your \(coffeeType.coffeeName)!")
            }
        }
    }
    
    func addAllIngridients() {
        addWater()
        addMilk()
        addBeans()
        cleanDregdrawer()
    }
    
    func addWater() {
        waterLevel = 2000
        print("Water added. Now the water level is \(waterLevel) ml.")
    }
    
    func addMilk() {
        milkLevel = 500
        print("Milk added. Now the milk level is \(milkLevel) ml.")
    }
    
    func addBeans() {
        beansLevel = 100
        print("Beans added. Now there are \(beansLevel) gramms of beans.")
    }
    
    func cleanDregdrawer() {
        dregdrawerLevel = 0
        print("Dregdrawer cleaned.")
    }
    
    func showLevels() {
        print("""
            ___________________________________________
            
            The level of water is \(waterLevel) ml.
            The level of milk is \(milkLevel) ml.
            The level of beans is \(beansLevel) gramms.
            The level of trash is \(dregdrawerLevel) of 10.
            ___________________________________________
            """)
    }
}

