//
//  CoffeeTypes.swift
//  CoffeeMachine
//
//  Created by Kirill Druzhynin on 16.06.2018.
//  Copyright © 2018 Kirill Druzhynin. All rights reserved.
//

import UIKit

class CoffeeTypes: NSObject {
    
    private static var totalCoffeesMade = 0
    private var _coffeesMade = 0
    
    var coffeeName: String
    let waterNeeded: Int
    let milkNeeded: Int
    let beansNeeded: Int
    let trashProduced: Int
    
    var coffeesCount: Int {
        return _coffeesMade
    }
    
    init(coffeeName: String, waterNeeded: Int, milkNeeded: Int, beansNeeded: Int, trashProduced: Int) {
        CoffeeTypes.totalCoffeesMade += 1
        self.coffeeName = coffeeName
        self.waterNeeded = waterNeeded
        self.milkNeeded = milkNeeded
        self.beansNeeded = beansNeeded
        self.trashProduced = trashProduced
    }
    
    func makeCoffee() {
        _coffeesMade += 1
    }
}
